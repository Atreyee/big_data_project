require 'rubygems'
require 'neography'

Neography.configure do |config|
  config.protocol       = "http://"
  config.server         = "localhost"
  config.port           = 7474
  config.directory      = ""  # prefix this path with '/'
  config.cypher_path    = "/cypher"
  config.gremlin_path   = "/ext/GremlinPlugin/graphdb/execute_script"
  config.log_file       = "neography.log"
  config.log_enabled    = false
  config.max_threads    = 20
  config.authentication = nil  # 'basic' or 'digest'
  config.username       = nil
  config.password       = nil
  config.parser         = MultiJsonParser
end

@neo = Neography::Rest.new

def degrees_of_separation(start_node, destination_node)
  paths =  @neo.get_paths(start_node,
                          destination_node,
                          {"type"=> "Link", "direction" => "out"},
                          depth=8,
                          algorithm="shortestPath")
  paths.each do |p|
    p["title"] = p["nodes"].collect { |node|
      @neo.get_node_properties(node, "title")["title"] }
  end
end

article_1 = @neo.get_node(2)

p "loaded data correctly"
p article_1["data"]["title"]
@neo.create_node_index("title", "exact")
@neo.create_node_auto_index
p @neo.list_node_indexes, "blah"
p @neo.get_node_index("node_auto_index", "title", "Anarchism"), "booooo"

#@neo.add_node_to_index("title", key, value, article_1)
#@neo.create_schema_index("document", ["title"])
#p @neo.get_schema_index(article_1)

article_2 = @neo.get_node(848052)

p "degree of separation"
p degrees_of_separation(article_1, article_2)

# nodes = @neo.traverse(article_1,                                              # the node where the traversal starts
#                       "nodes",                                            # return_type "nodes", "relationships" or "paths"
#                       {"order" => "breadth first",                        # "breadth first" or "depth first" traversal order

#                        "relationships" => [{"type"=> "Link",         # A hash containg a description of the traversal
#                                             "direction" => "out"}        # two relationships.
#                                            ],
#                        "prune evaluator" => {"language" => "javascript",  # A prune evaluator (when to stop traversing)
#                                              "body" => "position.endNode().getProperty('title') == 'Violin';"},
#                        "return filter" => {"language" => "builtin",       # "all" or "all but start node"
#                                            "name" => "all"},
#                         "depth" => 2
#                        })
# p nodes, "Here are your nodes"
# nodes.each do |n|
#   p n.title, "*"*20
# end

#article_2 = @neo.get_node(3) # get the node with title philodophy
#degrees_of_separation(article_1, article_2).each do |path|
#  puts "#{(path["title"].size - 1 )} degrees: " + path["title"].join(' => LINK => ')
#end