package org.graphipedia.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.neo4j.graphalgo.GraphAlgoFactory;
import org.neo4j.graphalgo.PathFinder;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.Expander;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.ReadableIndex;
import org.neo4j.kernel.Traversal;
import org.neo4j.tooling.GlobalGraphOperations;

public class GraphipediaServiceKarate {
    enum MyRelationshipTypes implements RelationshipType
    {
        FRIEND
    }
    private static final Expander FRIEND = Traversal.expanderForTypes(MyRelationshipTypes.FRIEND, Direction.BOTH);
    private final GraphDatabaseService db;
    private final ReadableIndex<Node> index;

    public GraphipediaServiceKarate(String storeDir) {
        Map<String, String> config = new HashMap<String, String>();
        config.put( "neostore.nodestore.db.mapped_memory", "100M" );
        config.put("neostore.relationshipstore.db.mapped_memory", "4G");
        config.put("neostore.propertystore.db.mapped_memory", "500M");
        config.put("neostore.propertystore.db.strings.mapped_memory", "150M");
        config.put("neostore.propertystore.db.arrays.mapped_memory", "160M");
        config.put("wrapper.java.initmemory", "256M");
        config.put("wrapper.java.maxmemory", "1024M");
        config.put("wrapper.java.additional", "-XX:+UseParallelGC");
        config.put("node_keys_indexable", "label" );
	    config.put("relationship_keys_indexable", "weight" );
	    config.put("node_auto_indexing", "true" );
	    config.put("relationship_auto_indexing", "true" );
		db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(storeDir).setConfig(config).newGraphDatabase();
        registerShutdownHook(db);
        index = db.index().getNodeAutoIndexer().getAutoIndex();
    }

	public void importData(){
		Node node1 = null;
		Relationship rel = null;
		Transaction tx = null;
		try {
			tx = db.beginTx();
			// Add indexable and non-indexable properties
			for (int i = 1; i <= 34; i++) {
				node1 = db.createNode();
				node1.setProperty("label", i);
				System.out.println(node1.getProperty("label") + " " + node1.getId());
			}


			Iterator<Entry<Integer, List<Integer>>> it = edges().entrySet()
					.iterator();
			while (it.hasNext()) {
				Entry<Integer, List<Integer>> e = it.next();
				int i1 = e.getKey();
				Node nodeA = findPerson(i1);
				List<Integer> list = e.getValue();
				Iterator<Integer> it1 = list.iterator();
				while(it1.hasNext()){
					int i2 = it1.next();
					System.out.println("source:"+i1+" target:"+i2);
					Node nodeB = findPerson(i2);
					rel = nodeA.createRelationshipTo(nodeB,MyRelationshipTypes.FRIEND);
					rel.setProperty("weight", 1);
				}

			}
			tx.success();
		} finally {
			tx.finish();
			db.shutdown();
		}
	}

    @SuppressWarnings("deprecation")
	public HashMap<Integer,List<List<Integer>>> findShortestPathFrom(int startLabel, int maxDepth){
    	Iterator<Node> it = db.getAllNodes().iterator();
    	HashMap<Integer, List<List<Integer>>> h = new HashMap<Integer, List<List<Integer>>>();
    	Node startNode = findPerson(startLabel);
    	System.out.println(startNode.getProperty("label"));

    	while(it.hasNext()){
    		Node endNode = it.next();
            if(endNode.getId() == 0)
            	endNode = it.next();
            PathFinder<Path> finder = GraphAlgoFactory.shortestPath(FRIEND, maxDepth);

            Iterable<Path> paths = finder.findAllPaths(startNode, endNode);
            List<List<Integer>> friendPaths = new ArrayList<List<Integer>>();
            int min = 999;
            for (Path path : paths) {
            	List<Integer> lables = extractLabels(path);
            	if(lables.size() < min)
            		min = lables.size();
            	friendPaths.add(lables);
            }

            System.out.println(min);
            h.put((Integer) endNode.getProperty("label"), friendPaths);
            System.out.println(endNode.getProperty("label"));
            System.out.println(friendPaths + "\n");
    	}
		return h;
    }

    private List<Integer> extractLabels(Path path) {
        if (path == null) {
            return null;
        }
        List<Integer> pages = new ArrayList<Integer>();
        for (Node node : path.nodes()) {
            pages.add((Integer) node.getProperty("label"));
        }
        return pages;
    }

    public Node findPerson(int startPage) {
        Node node = index.get("label", startPage).getSingle();
        if (node == null) {
            throw new IllegalArgumentException("no such page: " + startPage);
        }
        return node;
    }

    private void registerShutdownHook(final GraphDatabaseService db) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override public void run() {
                db.shutdown();
            }
        });
    }
    
    public HashMap<Integer, List<Integer>> edges(){
    	HashMap<Integer,List<Integer>> map = new HashMap<Integer, List<Integer>>();
    	map.put(2, new ArrayList<Integer>( Arrays.asList(1)));
    	
    	map.put(3, new ArrayList<Integer>( Arrays.asList(1,2)));
    	
    	map.put(26, new ArrayList<Integer>( Arrays.asList(24, 25)));
    	
    	map.put(22, new ArrayList<Integer>( Arrays.asList(1,2)));
    	map.put(20, new ArrayList<Integer>( Arrays.asList(1,2)));
    	
    	map.put(18, new ArrayList<Integer>( Arrays.asList(1,2)));

    	map.put(17, new ArrayList<Integer>( Arrays.asList(7,6)));
    	
    	map.put(14, new ArrayList<Integer>( Arrays.asList(1,2,3, 4)));
    	map.put(13, new ArrayList<Integer>( Arrays.asList(1,4)));
    	
    	map.put(12, new ArrayList<Integer>( Arrays.asList(1)));
    	
    	map.put(11, new ArrayList<Integer>( Arrays.asList(1,5,6)));

    	map.put(10, new ArrayList<Integer>( Arrays.asList(3)));

    	map.put(9, new ArrayList<Integer>( Arrays.asList(1,3)));


    	map.put(8, new ArrayList<Integer>( Arrays.asList(1,2, 3,4)));
    	
    	map.put(7, new ArrayList<Integer>( Arrays.asList(1,5, 6)));
    	
    	map.put(6, new ArrayList<Integer>( Arrays.asList(1)));
    	map.put(5, new ArrayList<Integer>( Arrays.asList(1)));
    	map.put(4, new ArrayList<Integer>( Arrays.asList(1,2, 3)));

    	map.put(34, new ArrayList<Integer>( Arrays.asList(33, 32, 31, 30, 29, 28, 27, 24, 23, 21, 20, 19, 16, 15, 14, 10, 9)));

    	map.put(33, new ArrayList<Integer>( Arrays.asList(32,31, 30, 24, 23, 21,19, 16, 15, 9, 3)));
    	
    	map.put(32, new ArrayList<Integer>( Arrays.asList(29, 26, 25, 1)));
    	
    	map.put(31, new ArrayList<Integer>( Arrays.asList(9,2)));
    	
    	map.put(30, new ArrayList<Integer>( Arrays.asList(27,24)));
    	
    	map.put(29, new ArrayList<Integer>( Arrays.asList(3)));
    	map.put(28, new ArrayList<Integer>( Arrays.asList(25,24,3)));
    	
    	return map;
    }


	public void printAllNodes() {
		Iterator<Node> it = GlobalGraphOperations.at(db).getAllNodes().iterator();
		while(it.hasNext()){
			Node node = it.next();
			if(node.getId() == 0){
				node = it.next();
			}
			System.out.println("node id: "+ node.getId() + " label: "+ node.getProperty("label"));
		}

	}


}
