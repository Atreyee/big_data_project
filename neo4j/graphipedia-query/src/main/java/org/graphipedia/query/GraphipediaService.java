//
// Copyright (c) 2012 Mirko Nasato
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
package org.graphipedia.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphalgo.GraphAlgoFactory;
import org.neo4j.graphalgo.PathFinder;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Expander;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.index.Index;
import org.neo4j.kernel.Traversal;

public class GraphipediaService {

    private static final Expander OUTGOING_LINKS = Traversal.expanderForTypes(WikiRelationshipType.Link, Direction.OUTGOING);

    private final GraphDatabaseService db;
    private final Index<Node> index;

    public GraphipediaService(String storeDir) {
        Map<String, String> config = new HashMap<String, String>();
        config.put( "neostore.nodestore.db.mapped_memory", "300M" );
        config.put("neostore.relationshipstore.db.mapped_memory", "5G");
        config.put("neostore.propertystore.db.mapped_memory", "1500M");
        config.put("neostore.propertystore.db.strings.mapped_memory", "750M");
        config.put("neostore.propertystore.db.arrays.mapped_memory", "1K");
        config.put("cache_type", "weak");
        config.put(".level", "FINEST");
        config.put("org.neo4j.server.level", "DEBUG");

		db = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(storeDir).setConfig(config).newGraphDatabase();
        registerShutdownHook(db);
        index = db.index().forNodes("pages");
    }

    public List<String> findPath(String startPage, String endPage, int maxDepth) {
        Node startNode = findPage(startPage);
        Node endNode = findPage(endPage);
        PathFinder<Path> finder = GraphAlgoFactory.shortestPath(OUTGOING_LINKS, maxDepth);
        Path path = finder.findSinglePath(startNode, endNode);
        return extractTitles(path);
    }

    public List<List<String>> findShortestPaths(String startPage, String endPage, int maxDepth) {
        Node startNode = findPage(startPage);
        Node endNode = findPage(endPage);
        PathFinder<Path> finder = GraphAlgoFactory.shortestPath(OUTGOING_LINKS, maxDepth);
        Iterable<Path> paths = finder.findAllPaths(startNode, endNode);
        
        List<List<String>> pagePaths = new ArrayList<List<String>>();
        int min = 999;
        for (Path path : paths) {
        	List<String> titles = extractTitles(path);
        	if(titles.size() < min)
        		min = titles.size();

        	pagePaths.add(titles);
        }
        System.out.println(min);
        return pagePaths;
    }
    
    @SuppressWarnings("deprecation")
	public HashMap<String, List<List<String>>> findShorteshPathTo(String endPage, int maxDepth){
    	Iterator<Node> it = db.getAllNodes().iterator();
    	HashMap<String, List<List<String>>> h = new HashMap<String, List<List<String>>>();
    	Node endNode = findPage(endPage);
    	System.out.println(endNode.getProperty("title"));
    	Long count = (long) 0;
    	while(it.hasNext()){
    		Node startNode = it.next();
            if(startNode.getId() == 0)
            	continue;
    		PathFinder<Path> finder = GraphAlgoFactory.shortestPath(OUTGOING_LINKS, maxDepth);

            Iterable<Path> paths = finder.findAllPaths(startNode, endNode);
            List<List<String>> pagePaths = new ArrayList<List<String>>();
            int min = 999;
            for (Path path : paths) {
            	List<String> titles = extractTitles(path);
            	if(titles.size() < min)
            		min = titles.size();
                pagePaths.add(titles);
            }
            
            System.out.println(min);
            h.put((String) startNode.getProperty("title"), pagePaths);
            System.out.println(startNode.getProperty("title"));
            System.out.println(pagePaths + "\n");
            count++;
            if(count == 20)
            	break;
    	}
    	return h;
    }
    
    
    public Node findPage(String title) {
        Node node = index.get("title", title).getSingle();
        
        if (node == null) {
            throw new IllegalArgumentException("no such page: " + title);
        }
        System.out.println("id is" + node.getId());
        return node;
    }

    private List<String> extractTitles(Path path) {
        if (path == null) {
            return null;
        }
        List<String> pages = new ArrayList<String>();
        for (Node node : path.nodes()) {
            pages.add((String) node.getProperty("title"));
        }
        return pages;
    }


    public void executeQuery(String string){

		ExecutionEngine engine = new ExecutionEngine( db );

        ExecutionResult result;
        result = engine.execute(string );
        System.out.println(result.toString());

    }

    private void registerShutdownHook(final GraphDatabaseService db) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override public void run() {
                db.shutdown();
            }
        });
    }

	public void findMostCitedNode() {
		@SuppressWarnings("deprecation")
		Iterator<Node> it = db.getAllNodes().iterator();

		long max = 0;
		Node node = null;
		while(it.hasNext()){

			Node endNode = it.next();
            if(endNode.getId() == 0)
            	continue;
            Long count = (long) 0;
            Iterator<Relationship> nodeRelIt = endNode.getRelationships(Direction.INCOMING).iterator();
            while(nodeRelIt.hasNext()){
            	count++;
            	nodeRelIt.next();
            }

            if(count > max){
            	max = count;
            	node = endNode;
            }
		}
        System.out.println(max);
        System.out.println(node.getProperty("title"));

	}

}
