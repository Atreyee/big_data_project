package org.graphipedia.query;

public class Query {

    public static void main(String[] args) {
        GraphipediaService queryService = new GraphipediaService("/Users/atreyeemaiti/CMU/15799/project/neo4j-wiki-with-talks/neo4j-community-1.9.4/data/graph.db");
        long startTime = System.currentTimeMillis();
        System.out.println("***************Six Degree of Separation******************************************");
        System.out.println(queryService.findPath("NewSQL", "Philosophy", 6));
        System.out.println(queryService.findPath("Pythagorean theorem", "Philosophy", 6));
        System.out.println(queryService.findPath("Adolf Hitler", "Philosophy", 6));
        System.out.println(queryService.findPath("Anarchism", "Philosophy", 6));
        System.out.println(queryService.findPath("Gene", "Philosophy", 6));
        System.out.println(queryService.findPath("Taj Mahal", "Philosophy", 6));
        System.out.println(queryService.findPath("Walking to the Sky", "Philosophy", 6));
        System.out.println(queryService.findPath("42", "Philosophy", 6));
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("total time for six degree in seconds: " + totalTime/1000);

        startTime = System.currentTimeMillis();
        System.out.println("*************Shortest path between a and b****************");
        System.out.println(queryService.findShortestPaths("Apple","Tansen", 10));
        System.out.println(queryService.findShortestPaths("Rajkot","Friends", 10));
        System.out.println(queryService.findShortestPaths("Water","Finale", 10));
        System.out.println(queryService.findShortestPaths("Shivaji","Snow", 10));
        System.out.println(queryService.findShortestPaths("Jim Gray (computer scientist)","Cotton", 10));
        endTime   = System.currentTimeMillis();
        totalTime = endTime - startTime;
        System.out.println("total time for shortest path in seconds: " + totalTime/1000);

        startTime = System.currentTimeMillis();
        System.out.println("*************Most cited document****************");
        queryService.findMostCitedNode();
        endTime   = System.currentTimeMillis();
        totalTime = endTime - startTime;
        System.out.println("total time for most cited in seconds: " + totalTime/1000);

//		System.out.println("***************Shortest Path Algorithm****************************************");
//		GraphipediaServiceKarate queryServiceForKarate = new GraphipediaServiceKarate("/Users/atreyeemaiti/CMU/15799/project/neo4j-community-1.9.4-football/data/graph.db");
//		//queryServiceForKarate.importData();
//		queryServiceForKarate.printAllNodes();
//		System.out.println(queryServiceForKarate.findShortestPathFrom(2, 6));
    
    }

}
